#!/usr/bin/perl
use strict;
use warnings;
use Cwd qw(abs_path);
use File::Basename;

our (%access, %text, %in, %config);
our ($module_name);

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}
if (! $d) {
  &error("Domain not found.");
}



# TODO: abstract to use with tiki-edit-ini.cgi
chdir($d->{'public_html_path'});
my $config = &tikimanager_tiki_get_config($d) || &error("Can't open local.php");
my $file = $config{'tiki_preferences_file'} || "../tikiconfig/prefs.ini";
if (-e $config->{'system_configuration_file'}) {
  $file = $config->{'system_configuration_file'};
}
my $identifier = 'tikiwiki';

eval_as_unix_user($d->{'user'}, sub { &useradmin::mkdir_if_needed(dirname($file)); });

my $data = $in{'data'};
$data =~ s/\r\n/\n/g;

if ($data =~ m/^ *\[([^\]]+) *\]/) {
  $identifier = $1;
}
else {
  $data = "[$identifier]\n$data";
}

my $SAVE;
&open_tempfile($SAVE, ">" . abs_path($file)) || &error("Can't save tiki ini file: " . &html_escape("$!"));
&print_tempfile($SAVE, $data);
&close_tempfile($SAVE);
chown($d->{'uid'}, $d->{'gid'}, $file);

$config->{'system_configuration_file'} = $file;
$config->{'system_configuration_identifier'} = $identifier;

&tikimanager_tiki_save_config($d, $config) || &error("local.php was not saved or corrupted");

if ($in{'save_close'}) {
  &redirect("index.cgi?dom=$d->{'id'}");
} else {
  &redirect("tiki-edit-ini.cgi?dom=$d->{'id'}");
}