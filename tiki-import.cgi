#!/usr/bin/perl
use strict;
use warnings;

our (%access, %text, %in);
our $module_name;

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

# apt-get install libemail-valid-perl
eval 'use Email::Valid;';

# check if user can access the page
# &can_domain($in{'dom'}) || &error($text{'contact_ecannot'});

my @errors;

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}

if (! $d) {
  push(@errors, "Domain not found.");
}
if (@errors) {
  &error(join('<br/>', @errors));
}


# Page title, must be first UI thing
&ui_print_header(
  'at ' . '<a href="https://' . $d->{'dom'} .'" target="_blank">https://' . $d->{'dom'} .'</a>',
  'Importing Tiki', "", undef, 1, 1
);

sub print_ln () {
  my ($str) = @_;
  $str =~ s/\s*$//;
  $str =~ s/</&lt;/;
  $str =~ s/>/&gt;/;
  print "$str\n";
}

&$virtual_server::first_print("Installing Tiki using Tiki Manager..");
print '<pre style="white-space: pre-wrap; background-color: black; color: white;">' . "\n";
&tikimanager_tiki_import($d, \&print_ln);
print '</pre>' . "\n";
&$virtual_server::second_print(".. done");

&$virtual_server::first_print("Setting up cron jobs for Tiki..");
&tikimanager_cron_setup($d);
&$virtual_server::second_print(".. done");

&ui_print_footer(
  $d ? &virtual_server::domain_footer_link($d) : ( ),
  "index.cgi?dom=$in{'dom'}",
  $text{'index_the_information_page'}
);
