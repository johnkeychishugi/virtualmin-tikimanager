#!/usr/bin/perl

use warnings;

sub get_php_bin
{
    my $php;

    $php = `which php 2>&-`;
    if ($php) {
        $php =~ s/\s*$//;
        return $php;
    }
    return 0;
}


sub check_php_version
{
    my $php = get_php_bin;
    my $major_version = `$php -r 'echo PHP_MAJOR_VERSION;' 2>&1`;
    my $minor_version = `$php -r 'echo PHP_MINOR_VERSION;' 2>&1`;

    if ($major_version == 7 && $minor_version >= 4) {
        return 1;
    }

    return 0;
}

if (! (get_php_bin && check_php_version)) {
    exit 1;
}
